import { ControlInterface, StateInfoWithValue, TransitionWithControlCounter } from "../fsm/automat-with-control"
import { InputInterface, StateInterface, Transition } from "../fsm/basics"
import { StateValueControllerInterface, StateValueInterface } from "../fsm/state-value"

export class StateValue implements StateValueInterface {
    value:number
    comment?:string
    constructor (value:number, comment?:string) {
        this.value = value
        this.comment = comment
    }
}
export class StateValueController implements StateValueControllerInterface <StateValue> {
    protected possibleValues : number [] = []
    protected possibleStateValues : StateValue []
    constructor (possibleStateValues:StateValue []) {
        if (possibleStateValues.length == 0)
            throw Error('No possible states')
        possibleStateValues.sort(this.compareStateValues)
        this.possibleStateValues = possibleStateValues
        for (let i = 0; i < possibleStateValues.length; i++)
            this.possibleValues.push(possibleStateValues[i].value)
    }
    public getStateValue = (value:number) : StateValue | null => {
        let index = this.possibleValues.indexOf(value)
        if (index == -1)
            return null

        return this.possibleStateValues[index]
    }
    protected compareStateValues = (first:StateValue, second:StateValue) : number => {
        if (first.value == second.value)
            return 0

        if (first.value < second.value)
            return -1

        return 1
    }
    public getStateWithMaxValue = () : StateValue => {
        return this.possibleStateValues[this.possibleStateValues.length - 1]
    }
    public getStateWithMinValue = () : StateValue => {
        return this.possibleStateValues[0]
    }
    public getPreviousState = (active:StateValue) : StateValue | null => {
        let index = this.possibleValues.indexOf(active.value)
        if (index == -1)
            throw Error('Such state was not found')

        if (index == 0)
            return null

        return this.possibleStateValues[index - 1]
    }
    public getNextState = (active:StateValue) : StateValue | null => {
        let index = this.possibleValues.indexOf(active.value)
        if (index == -1)
            throw Error('Such state was not found')

        if (index == this.possibleStateValues.length - 1)
            return null

        return this.possibleStateValues[index + 1]
    }
}
export class State implements StateInfoWithValue <StateValue> {
    id:string
    value:StateValue
    constructor (id:string, value?:StateValue) {
        this.id = id
        if (value)
            this.value = value
        else    
            this.value = new StateValue(0)
    }
    equals = (state:StateInterface) => {
        if (this.id == state.id)
            return true

        return false
    }
    getInfo = () => {
        return {
            id:this.id,
            value:this.value.value
        }
    }
}
export enum InputValue {
    False = 0,
    Maybe = 1,
    True = 2
}
export class Input implements InputInterface {
    value:InputValue
    id:string
    constructor (id:string, value?:InputValue) {
        this.value = value || InputValue.False
        this.id = id
    }
    equals = (input:Input) => {
        if (input.id != this.id)
            return false

        if (input.value != this.value)
            return false

        return true
    }
    getInfo = () => {
        return {
            id:this.id,
            value:this.value
        }
    }
}
export class MyTransitionCounter extends TransitionWithControlCounter <StateValue, StateValueController, State, Input, ControlInterface <Input>> {
    countTransition = (activeState: State, input: Input, control?:ControlInterface <Input>) : State => {
        let activeTransitions = this.transitions[activeState.id]

        if (!activeTransitions)
            return activeState

        for (let i = 0; i < activeTransitions.length; i++)
            if (activeTransitions[i].input.equals(input)) {
                if (control?.checkAllowed(input, activeState))
                    return activeTransitions[i].to
            }

        return activeState
    }

}
export class MyTransition implements Transition <State, Input> {
    from: State
    to: State
    input: Input
    constructor (from:State, to:State, input:Input) {
        this.from = from
        this.to = to
        this.input = input
    }
}