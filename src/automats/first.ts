import { AutomatCreator } from "../creator"
import { ControlInterface } from "../fsm/automat-with-control"
import {  InputInterface, StateInterface, Transition } from "../fsm/basics"
import * as fs from "fs"
import { FullAutomat, ProtectedOutputInterface } from "../fsm/automat-with-outputs"
import { Input, InputValue, MyTransition, MyTransitionCounter, State, StateValue, StateValueController } from "./base"
const ENCODING = 'utf-8'

class FirstOutput extends ProtectedOutputInterface <{ Y1:boolean }> {
    toString = () => {
        let result = ''
        if (this.values.Y1)
            result += 'Y1'
        return result
    }
}
class MyFirstControl extends ControlInterface <InputInterface> {
    public updateStateControl =(state: StateInterface, inputs: InputInterface[]) => {
        /*let allowedIds = this.getAllowedIds(state)
        let allowedInputs = this.getAllowedInputs(state)
        for (let i = 0; i < inputs.length; i++) {
            let index = allowedIds.indexOf(inputs[i].id)
            if (index > -1) {
                //exist, remove
                allowedInputs.splice(index, 1)
            }
            else {
                allowedInputs.push(inputs[i])
            }
        }
        this.allowedInputs[state.id] = allowedInputs*/
        this.allowedInputs[state.id] = [...inputs]
    }
}

export class MyFirstAutomat extends FullAutomat <StateValue, StateValueController, State, Input, ControlInterface <Input>, FirstOutput> {
    constructor (states : State [], transitions : Transition <State, Input> [], defaultState : State, stateValueController:StateValueController, defaultControl:ControlInterface <Input>) {
        super(states, transitions, defaultState)
        this.control = defaultControl
        this.transitionCounter = new MyTransitionCounter(transitions, stateValueController)
        this.activeState = defaultState
        this.activeState.value = new StateValue(2)
        this.syncState(this.activeState)
        this.previousState = this.activeState
        for (let i = 0; i < this.states.length; i++)
            if (!this.states[i].equals(this.activeState))
                this.states[i].value = new StateValue(0)
    }
    public count = (input: Input, controlUpdate?:Input []) => {
        if (controlUpdate)
            this.control.updateStateControl(this.activeState, controlUpdate)
        if (!input || input.value == InputValue.False) { // нет икса 
            this.previousState = this.activeState
            return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.activeState) || [], previousState:this.previousState.getInfo(), activeState:this.activeState.getInfo(), changed:false, states:this.getStates() }
        }
        let nextState = this.transitionCounter.countTransition(this.activeState, input, this.control)
        if (this.activeState.equals(nextState)) {// замкнулось в это же состояние, возможно баг
            this.previousState = this.activeState
            return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.activeState) || [], previousState:this.previousState.getInfo(), activeState:this.activeState.getInfo(), changed:false, states:this.getStates() }
        }
        this.updateStatesValues(nextState, input)

        //проверить - возможно десинхрон
        if (!this.previousState)    
            throw Error('Previous state undefined')
        return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.previousState) || [], previousState:this.previousState.getInfo(), activeState:this.activeState.getInfo(), changed:true, states:this.getStates() }
    }
    countOutput = (input:Input) : FirstOutput => {
        if (!this.activeState.equals(new State('S0')))
            return new FirstOutput({ Y1:false })
        if (this.previousState?.equals(new State('S0')))
            return new FirstOutput({ Y1:false })

        return new FirstOutput({ Y1:true })
    }
    updateStatesValues = (nextStateS:State, input:Input) => {
        let nextState : State | undefined = { ...nextStateS }
        let previousState : State | undefined = this.previousState ? { ...this.previousState } : undefined
        let activeState : State = { ...this.activeState }
        if (input.value == InputValue.False) {
           throw Error('Inactive input')
        } else if (input.value == InputValue.Maybe) {
            nextState.value = new StateValue(1)
        } else if (input.value == InputValue.True) {
            previousState = activeState
            previousState.value = new StateValue(1)
            activeState = nextState
            activeState.value = new StateValue(2)
            for (let i = 0; i < this.states.length; i++)
                this.states[i].value = new StateValue(0)
            nextState = undefined
        }
        this.previousState = previousState
        if (previousState)
            this.syncState(previousState)
        this.activeState = activeState
        this.syncState(activeState)

        if (nextState)
            this.syncState(nextState)
    }
}

export class MyFirstCreator extends AutomatCreator {
    protected outputFileName?:string
    constructor (inputsFileName:string, statesFileName:string, transitionsFileName:string, controlsFileName:string, needsCount:boolean, outputFileName?:string) {
        super()
        this.outputFileName = outputFileName
        this.createStates(statesFileName)
        this.createTransitions(transitionsFileName)
        this.createActions(inputsFileName)
        this.createDefaultControl(controlsFileName)
        const stateValueController = new StateValueController([
            { value:0, comment:'Inactive' },
            { value:1, comment:'Maybe' },
            { value:1, comment:'Active' }
        ])
        this.automat = new MyFirstAutomat(this.states, this.transitions, this.states[0], stateValueController, this.control)
        if (!needsCount)
            return;
        this.count()
        if (!outputFileName)
            this.printResults()
        else
            this.writeFile()
    }
    createStates = (filename:string) => {
        //format - every string is state name
        let fileData = fs.readFileSync(filename, ENCODING)
        let stateNames = fileData.split('\n')
        for (let i = 0; i < stateNames.length; i++)
            this.states.push(new State(stateNames[i].trim()))
    }
    createTransitions = (filename:string) => {
        let fileData = fs.readFileSync(filename, ENCODING)
        let transitionsInfo = fileData.split('\n')
        for (let i = 0; i < transitionsInfo.length; i++) {
            //format:<stateFrom> <stateTo> <inputId> <value>
            let oneTransitionInfo = transitionsInfo[i].split(' ')
            if (oneTransitionInfo.length != 4)  
                throw Error('False transition data length ' + i)

            let fromStateId = oneTransitionInfo[0].trim()
            let toStateId = oneTransitionInfo[1].trim()
            let inputId = oneTransitionInfo[2].trim()
            let inputValue = parseInt(oneTransitionInfo[3].trim())

            if (!Object.values(InputValue).includes(inputValue))
                throw Error('False input value ' + i)
            else
                inputValue = inputValue as InputValue

            this.transitions.push(new MyTransition(new State(fromStateId), new State(toStateId), new Input(inputId, inputValue)))
        }
    }
    createDefaultControl = (controlsFileName:string) => {
        let costrolsFileData = fs.readFileSync(controlsFileName, ENCODING)
        let controlsInfo = costrolsFileData.split('\n')
        let allowedInputs : { [key:string] : Input [] } = {}
        for (let i = 0; i < controlsInfo.length; i++) {
            let oneControlInfo = controlsInfo[i]
            let allowedInfos = oneControlInfo.split(' ')
            let stateId = allowedInfos[0]
            for (let i = 1; i < allowedInfos.length; i++) {
                if (!allowedInputs[stateId])
                    allowedInputs[stateId] = []

                allowedInputs[stateId].push(new Input(allowedInfos[i].trim()))
            }
        }

        this.control = new MyFirstControl(allowedInputs)
    }
    createActions = (inputsFileName:string) => {
        let actionsFileData = fs.readFileSync(inputsFileName, ENCODING)

        let inputsInfo = actionsFileData.split('\n')
        /*if (inputsInfo.length !== controlsInfo.length)
            throw Error('False inputs and controls info length - not equals')*/


        for (let i = 0; i < inputsInfo.length; i++) {
            //inputs format:<id> <value> <allowedXId> <allowedXId> ...
            let oneInputInfo = inputsInfo[i].split(' ')
            if (oneInputInfo.length < 2) {
                throw Error('False input info length ' + i)
            }
            
            let inputId = oneInputInfo[0].trim()
            let inputValue = parseInt(oneInputInfo[1])

            if (!Object.values(InputValue).includes(inputValue))
                throw Error('False input value ' + i)
            else
                inputValue = inputValue as InputValue
            
            let input = new Input(inputId, inputValue)
            let controlUpdate : Input[] | undefined

            if (oneInputInfo.length > 2) {
                //available controls override
                controlUpdate = []
                for (let j = 2; j < oneInputInfo.length; j++)
                    controlUpdate?.push(new Input(oneInputInfo[j].trim()))
            }

            this.actions.push({
                input,
                controlUpdate
            })
        }
        //console.log(this.actions)
    }
    writeFile = () => {
        let MAX_N_LENGTH = 7
        let resultString = '|   №   ||  X   | Value ||'
        for (let i = 0; i < this.states.length; i++)
            resultString += '  ' + this.states[i].id + ' |'

        resultString += '| Allowed x (before transition)'
        resultString += '\n'

        for (let i = 0; i < this.results.length; i++) {
            resultString += '|'
            let iWrite = (i+1).toString()
            let padding = Math.ceil((MAX_N_LENGTH - iWrite.length) / 2)
            for (let j = 0; j < padding; j++)
                resultString+=' '
            resultString += iWrite
            padding = MAX_N_LENGTH - iWrite.length - padding
            for (let j = 0; j < padding; j++)
                resultString+=' '
            resultString += '||'
            //resultString += '| ' + (i+1) + ' ||'
            resultString += '  ' + this.results[i].input.id + '  |'
            resultString += '   ' + this.results[i].input.value + '   ||'
            for (let j = 0; j < this.states.length; j++)
                resultString += '  ' + this.results[i].states[j].value + '  |'  
            resultString += '| '
            for (let j = 0; j < this.results[i].allowedInputs.length; j++) {
                resultString += this.results[i].allowedInputs[j]
                if (j != this.results[i].allowedInputs.length-1)
                    resultString += ' '
            }
            if (i != this.results.length - 1) 
                resultString += '\n'
        }

        if (!this.outputFileName)
            throw Error('File name was not provided')
        fs.writeFileSync(this.outputFileName, resultString, { encoding:ENCODING })
    }
    getAutomat = () => {
        return this.automat as MyFirstAutomat
    }
}