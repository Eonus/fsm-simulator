import { AutomatCreator } from "../creator"
import { ControlInterface, StateInfoWithValue, TransitionWithControlCounter } from "../fsm/automat-with-control"
import { InputInterface, StateInterface, Transition } from "../fsm/basics"
import { StateValueControllerInterface, StateValueInterface } from "../fsm/state-value"
import * as fs from "fs"
import { CountResultWithOutput, FullAutomat, ProtectedOutputInterface } from "../fsm/automat-with-outputs"
import { Input, MyTransition, MyTransitionCounter, State, StateValue, StateValueController } from "./base"
const ENCODING = 'utf-8'

export class SecondOutput extends ProtectedOutputInterface <{ [key:string]:boolean }> {
    toString = () => {
        let result = ''
        for (let key in this.values) {
            if (this.values[key])
                result += key + ' '
        }
        if (result[result.length - 1] == ' ')
            result = result.slice(0, -1)
        return result
    }
}

enum InputValue {
    False = 0,
    True = 2
}

class MySecondControl extends ControlInterface <InputInterface> {
    public updateStateControl =(state: StateInterface, inputs: InputInterface[]) => {
        this.allowedInputs[state.id] = [...inputs]
    }
}

export class MySecondAutomat extends FullAutomat <StateValue, StateValueController, State, Input, ControlInterface <Input>, SecondOutput> {
    protected outputs:{ [key:string]:SecondOutput }
    constructor (states : State [], transitions : Transition <State, Input> [], defaultState : State, stateValueController:StateValueController, defaultControl:ControlInterface <Input>, outputs:{ [key:string]:SecondOutput }) {
        super(states, transitions, defaultState)
        this.control = defaultControl
        this.transitionCounter = new MyTransitionCounter(transitions, stateValueController)
        this.activeState = defaultState
        this.activeState.value = new StateValue(2)
        this.syncState(this.activeState)
        this.previousState = this.activeState
        for (let i = 0; i < this.states.length; i++)
            if (!this.states[i].equals(this.activeState))
                this.states[i].value = new StateValue(0)
        this.outputs = outputs
    }
    public count = (input: Input, controlUpdate?:Input []) : CountResultWithOutput<SecondOutput> => {
        if (controlUpdate)
            this.control.updateStateControl(this.activeState, controlUpdate)
        if (!input || input.value == InputValue.False) // нет икса 
            return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.activeState) || [], previousState:this.activeState.getInfo(), activeState:this.activeState.getInfo(), changed:false, states:this.getStates() }

        let nextState = this.transitionCounter.countTransition(this.activeState, input, this.control)
        if (this.activeState.equals(nextState)) // замкнулось в это же состояние, возможно баг
            return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.activeState) || [], previousState:this.activeState.getInfo(), activeState:this.activeState.getInfo(), changed:false, states:this.getStates() }
        
        this.updateStatesValues(nextState, input)

        //проверить - возможно десинхрон
        if (!this.previousState)    
            throw Error('Previous state undefined')
        return { input:input.getInfo(), output:this.countOutput(input), allowedInputs:this.control.getAllowedIds(this.previousState) || [], previousState:this.previousState.getInfo(), activeState:this.activeState.getInfo(), changed:true, states:this.getStates() }
    }
    protected countOutput = (input: Input) : SecondOutput => {
        if (this.outputs[this.activeState.id])
            return this.outputs[this.activeState.id]
        
        throw Error('Output is not defined for state ' + this.activeState.id)
    }
    protected updateStatesValues = (nextStateS: State, input: Input) => {
        let nextState : State | undefined = { ...nextStateS }
        let previousState : State | undefined = this.previousState ? { ...this.previousState } : undefined
        let activeState : State = { ...this.activeState }
        if (input.value == InputValue.False) {
           throw Error('Inactive input')
        } else if (input.value == InputValue.True) {
            previousState = activeState
            previousState.value = new StateValue(0)
            activeState = nextState
            activeState.value = new StateValue(2)
            for (let i = 0; i < this.states.length; i++)
                this.states[i].value = new StateValue(0)
            nextState = undefined
        }
        this.previousState = previousState
        if (previousState)
            this.syncState(previousState)
        this.activeState = activeState
        this.syncState(activeState)

        if (nextState)
            this.syncState(nextState)
    }
}

export class MySecondCreator extends AutomatCreator {
    protected outputs:{ [key:string]:SecondOutput } = {}
    protected outputFileName?:string
    constructor (inputsFileName:string, statesFileName:string, transitionsFileName:string, controlsFileName:string, needsCount:boolean, outputFileName?:string) {
        super()
        this.outputFileName = outputFileName
        this.createStates(statesFileName)
        this.createTransitions(transitionsFileName)
        this.createActions(inputsFileName)
        this.createDefaultControl(controlsFileName)
        const stateValueController = new StateValueController([
            { value:0, comment:'Inactive' },
            { value:1, comment:'Maybe' },
            { value:1, comment:'Active' }
        ])
        this.automat = new MySecondAutomat(this.states, this.transitions, this.states[0], stateValueController, this.control, this.outputs)
        if (!needsCount)
            return;
        this.count()
        if (!outputFileName)
            this.printResults()
        else
            this.writeFile()
    }
    createStates = (filename:string) => {
        //format - every string is state name
        let fileData = fs.readFileSync(filename, ENCODING)
        let stateInfos = fileData.split('\n')
        for (let i = 0; i < stateInfos.length; i++) {
            let oneStateInfo = stateInfos[i].split(' ')
            let stateName = oneStateInfo[0].trim()
            let defaultOutputs : { [key:string]:boolean } = {}
            if (oneStateInfo.length < 1)
                throw Error('False state info length')

            for (let j = 1; j < oneStateInfo.length; j++)
                defaultOutputs[oneStateInfo[j].trim()] = true

            let secondOutput = new SecondOutput(defaultOutputs)
            this.states.push(new State(stateName))
            this.outputs[stateName] = secondOutput
        }
    }
    createTransitions = (filename:string) => {
        let fileData = fs.readFileSync(filename, ENCODING)
        let transitionsInfo = fileData.split('\n')
        for (let i = 0; i < transitionsInfo.length; i++) {
            //format:<stateFrom> <stateTo> <inputId> <value>
            let oneTransitionInfo = transitionsInfo[i].split(' ')
            if (oneTransitionInfo.length != 4)  
                throw Error('False transition data length ' + i)

            let fromStateId = oneTransitionInfo[0].trim()
            let toStateId = oneTransitionInfo[1].trim()
            let inputId = oneTransitionInfo[2].trim()
            let inputValue = parseInt(oneTransitionInfo[3].trim())

            if (!Object.values(InputValue).includes(inputValue))
                throw Error('False input value ' + i)
            else
                inputValue = inputValue as InputValue

            this.transitions.push(new MyTransition(new State(fromStateId), new State(toStateId), new Input(inputId, inputValue)))
        }
    }
    createDefaultControl = (controlsFileName:string) => {
        let costrolsFileData = fs.readFileSync(controlsFileName, ENCODING)
        let controlsInfo = costrolsFileData.split('\n')
        let allowedInputs : { [key:string] : Input [] } = {}
        for (let i = 0; i < controlsInfo.length; i++) {
            let oneControlInfo = controlsInfo[i]
            let allowedInfos = oneControlInfo.split(' ')
            let stateId = allowedInfos[0]
            for (let i = 1; i < allowedInfos.length; i++) {
                if (!allowedInputs[stateId])
                    allowedInputs[stateId] = []

                allowedInputs[stateId].push(new Input(allowedInfos[i].trim()))
            }
        }

        this.control = new MySecondControl(allowedInputs)
    }
    createActions = (inputsFileName:string) => {
        let actionsFileData = fs.readFileSync(inputsFileName, ENCODING)

        let inputsInfo = actionsFileData.split('\n')
        /*if (inputsInfo.length !== controlsInfo.length)
            throw Error('False inputs and controls info length - not equals')*/


        for (let i = 0; i < inputsInfo.length; i++) {
            //inputs format:<id> <value> <allowedXId> <allowedXId> ...
            let oneInputInfo = inputsInfo[i].split(' ')
            if (oneInputInfo.length < 2) {
                throw Error('False input info length ' + i)
            }
            
            let inputId = oneInputInfo[0].trim()
            let inputValue = parseInt(oneInputInfo[1])

            if (!Object.values(InputValue).includes(inputValue))
                throw Error('False input value ' + i)
            else
                inputValue = inputValue as InputValue
            
            let input = new Input(inputId, inputValue)
            let controlUpdate : Input[] | undefined

            if (oneInputInfo.length > 2) {
                //available controls override
                controlUpdate = []
                for (let j = 2; j < oneInputInfo.length; j++)
                    controlUpdate?.push(new Input(oneInputInfo[j].trim()))
            }

            this.actions.push({
                input,
                controlUpdate
            })
        }
        //console.log(this.actions)
    }
    writeFile = () => {
        let MAX_N_LENGTH = 7
        let resultString = '|   №   ||  X   | Value ||'
        for (let i = 0; i < this.states.length; i++)
            resultString += '  ' + this.states[i].id + ' |'

        resultString += '| Allowed x (before transition)'
        resultString += '\n'

        for (let i = 0; i < this.results.length; i++) {
            resultString += '|'
            let iWrite = (i+1).toString()
            let padding = Math.ceil((MAX_N_LENGTH - iWrite.length) / 2)
            for (let j = 0; j < padding; j++)
                resultString+=' '
            resultString += iWrite
            padding = MAX_N_LENGTH - iWrite.length - padding
            for (let j = 0; j < padding; j++)
                resultString+=' '
            resultString += '||'
            //resultString += '| ' + (i+1) + ' ||'
            resultString += '  ' + this.results[i].input.id + '  |'
            resultString += '   ' + this.results[i].input.value + '   ||'
            for (let j = 0; j < this.states.length; j++)
                resultString += '  ' + this.results[i].states[j].value + '  |'  
            resultString += '| '
            for (let j = 0; j < this.results[i].allowedInputs.length; j++) {
                resultString += this.results[i].allowedInputs[j]
                if (j != this.results[i].allowedInputs.length-1)
                    resultString += ' '
            }
            if (i != this.results.length - 1) 
                resultString += '\n'
        }

        if (!this.outputFileName)
            throw Error('File name was not provided')
        fs.writeFileSync(this.outputFileName, resultString, { encoding:ENCODING })
    }
    getAutomat = () => {
        return this.automat as MySecondAutomat
    }
}