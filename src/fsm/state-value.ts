export interface StateValueInterface {
    value:number
    comment?:string
}

export interface StateValueControllerInterface <StateValue> {
    getStateValue : (value:number) => StateValue | null
    getStateWithMaxValue : () => StateValue
    getStateWithMinValue : () => StateValue
    getPreviousState : (active:StateValue) => StateValue | null
    getNextState : (active:StateValue) => StateValue | null
}