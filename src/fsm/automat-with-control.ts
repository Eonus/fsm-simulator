import { AutomatAbstract } from "./automat"
import { CountResult, InputInterface, StateInterface, Transition } from "./basics"
import { TransitionCounter } from "./transition-counter"
import { StateValueControllerInterface } from './state-value'

export interface StateInfoWithValue <StateValue> extends StateInterface {
    value:StateValue
}

export abstract class ControlInterface <Input extends InputInterface> {
    protected allowedInputs : { [key:string] : Input [] }
    constructor (allowedInputs : { [key:string] : Input [] }) {
        this.allowedInputs = allowedInputs
    }
    checkAllowed = (input:Input, state:StateInterface) => {
        let allowedInputs = this.allowedInputs[state.id]
        if (!allowedInputs)
            return false

        for (let i = 0; i < allowedInputs.length; i++)
            if (allowedInputs[i].id == input.id)
                return true

        return false
    }
    getAllowedIds = (state:StateInterface) => {
        let ids = []
        let allowedInputs = this.allowedInputs[state.id]
        if (!allowedInputs)
            return []
        for (let i = 0; i < allowedInputs.length; i++)
            ids.push(allowedInputs[i].id)
        return ids
    }
    getAllowedInputs = (state:StateInterface) => {
        let inputs = []
        let allowedInputs = this.allowedInputs[state.id]
        if (!allowedInputs)
            return []
        for (let i = 0; i < allowedInputs.length; i++)
            inputs.push(allowedInputs[i])
        return inputs
    }
    public abstract updateStateControl : (state:StateInterface, inputs: Input[]) => void
}

export abstract class TransitionWithControlCounter <StateValue,StateValueController extends StateValueControllerInterface <StateValue>, State extends StateInfoWithValue <StateValue>, Input extends InputInterface, Control extends ControlInterface<Input>> extends TransitionCounter <State, Input> {
    protected stateValueController:StateValueController
    constructor (transitions:Transition <State, Input> [], stateValueController:StateValueController) {
        super(transitions)
        this.stateValueController = stateValueController
    }
    abstract countTransition : (activeState:State, input:Input, control?:Control) => State
}

export abstract class AutomatWithControl <StateValue, StateValueController extends StateValueControllerInterface <StateValue>, State extends StateInfoWithValue <StateValue>, Input extends InputInterface, Control extends ControlInterface<Input>> extends AutomatAbstract <State, Input> {
    protected transitionCounter!:TransitionWithControlCounter <StateValue, StateValueController, State, Input, Control>
    protected control!:Control
    protected previousState?:State
    public abstract count : (input:Input, controlUpdate?:Input []) => CountResult
    protected abstract updateStatesValues : (nextState:State, input:Input) => void
}
