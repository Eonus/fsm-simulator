import { InputInterface, StateInterface, Transition } from "./basics"

export abstract class TransitionCounter <State extends StateInterface, Input extends InputInterface> {
    protected transitions : { [key:string] : Transition <State, Input> [] } = { }
    constructor (transitions:Transition <State, Input> []) {
        this.initTransitions(transitions)
    }
    protected initTransitions = (transitions:Transition <State, Input> []) => {
        for (let i = 0; i < transitions.length; i++) {
            if (!this.transitions[transitions[i].from.id])
                this.transitions[transitions[i].from.id] = []

            this.transitions[transitions[i].from.id].push(transitions[i])
        }
    }
    abstract countTransition : (activeState:State, input:Input) => State
}