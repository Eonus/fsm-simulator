import { CountResult, InputInterface, StateInterface, Transition } from "./basics"
import { TransitionCounter } from "./transition-counter"

export abstract class AutomatAbstract <State extends StateInterface, Input extends InputInterface> {
    protected transitionCounter !: TransitionCounter <State, Input>
    protected transitions : Transition <State, Input> []
    protected states : State []
    protected activeState : State

    constructor (states : State [], transitions : Transition <State, Input> [], defaultState : State) {
        this.states = states
        this.transitions = transitions
        this.activeState = defaultState
    }

    protected syncState = (state:State) => {
        for (let i = 0; i < this.states.length; i++)
            if (this.states[i].equals(state)) {
                this.states[i].value = state.value
                return
            }

        throw Error('This state is not in states')
    }

    protected getStates = () => {
        let info = []
        for (let i = 0; i < this.states.length; i++)
            info.push(this.states[i].getInfo())
        return info
    }

    public abstract count : (input:Input, ...params:any) => CountResult
}