import { AutomatAbstract } from "./automat";
import { AutomatWithControl, ControlInterface, StateInfoWithValue } from "./automat-with-control";
import { CountResult, InputInterface, StateInterface } from "./basics";
import { StateValueControllerInterface } from "./state-value";

export interface OutputInterface {
    toString():string
}

export abstract class ProtectedOutputInterface <Base extends { [key:string] : any }> implements OutputInterface {
    protected values : Base
    constructor (defaultValues:Base) {
        this.values = defaultValues
    }
    getValue  = <ID extends keyof Base> (id:ID) => {
        return this.values[id]
    }
    setValue  = <ID extends keyof Base> (id:ID, value:Base[ID]) => {
        this.values[id] = value
    }
    getValues = () : Base => {
        return this.values
    }
}

export interface CountResultWithOutput <Output extends OutputInterface> extends CountResult {
    output:Output
}

export abstract class AutomatWithOutput <State extends StateInterface, Input extends InputInterface, Output extends OutputInterface> extends AutomatAbstract <State, Input> {
    public abstract count : (input:Input, ...params:any) => CountResultWithOutput<Output>
}

export abstract class FullAutomat <StateValue, StateValueController extends StateValueControllerInterface <StateValue>, State extends StateInfoWithValue <StateValue>, Input extends InputInterface, Control extends ControlInterface<Input>, Output extends OutputInterface> extends AutomatWithControl <StateValue, StateValueController, State, Input, Control> {
    public abstract count : (input:Input, ...params:any) => CountResultWithOutput<Output>
    protected abstract countOutput : (input:Input) => Output
}