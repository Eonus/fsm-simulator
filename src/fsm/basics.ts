export interface CountResult {
    input:{ id:string, value:any }
    previousState:StateInfo
    activeState:StateInfo
    changed:boolean
    states : StateInfo []
    allowedInputs : String []
}

export interface StateInfo {
    id : string
    value : any
}

export interface StateInterface extends Comparable <StateInterface> {
    id : string
    value : any
    getInfo () : StateInfo
}

export interface Transition <State extends StateInterface, Input extends InputInterface> {
    from : State
    to : State
    input : Input
}

interface Comparable <T> {
    equals (input:T) : boolean
}

export interface InputInterface extends Comparable <InputInterface> {
    id : string
    value : any
    getInfo : () => { id:string, value:any }
}