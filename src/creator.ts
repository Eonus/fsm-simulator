import {  AutomatWithControl, ControlInterface, StateInfoWithValue } from './fsm/automat-with-control'
import { CountResult, InputInterface, Transition } from './fsm/basics'

export abstract class AutomatCreator {
    protected states : StateInfoWithValue <any> [] = []
    protected transitions : Transition <any, any> [] = []
    protected automat !: AutomatWithControl <any, any, any, any, any>
    protected actions : { input:InputInterface, controlUpdate?: InputInterface []}[] = []
    public results : CountResult[] = []
    protected control !: ControlInterface <any>
    protected abstract createStates : (...params:any) => void
    protected abstract createTransitions : (...params:any) => void
    protected abstract createActions : (...params:any) => void
    protected abstract createDefaultControl : (...params:any) => void
    public abstract writeFile : () => void
    protected init = () => {
        this.createStates()
        this.createTransitions()
        this.createDefaultControl()
        this.createActions()
    }
    protected count = () => {
        for (let i = 0; i < this.actions.length; i++)
            this.results.push(this.automat.count(this.actions[i].input, this.actions[i]?.controlUpdate))
    }
    public printResults = () => {
        for (let i = 0; i < this.results.length; i++)
            console.log(this.results[i])
    }
}