import { Input, InputValue } from "./automats/base"
import { MyFirstAutomat, MyFirstCreator } from "./automats/first"
import { MySecondAutomat, MySecondCreator, SecondOutput } from "./automats/seconds"

const rand = (min:number, max:number) => {
    let rand = min - 0.5 + Math.random() * (max - min + 1);
    return Math.round(rand);
}

class Counter {
    protected first:MyFirstAutomat
    protected second:MySecondAutomat
    protected lastSecondOutput?:SecondOutput
    protected firstCreator:MyFirstCreator
    protected secondCreator:MySecondCreator
    
    constructor () {
        this.firstCreator = new MyFirstCreator('data/first/inputs.txt', 'data/first/states.txt', 'data/first/transitions.txt', 'data/first/controls.txt', false, 'data/first/output.txt')
        this.secondCreator = new MySecondCreator('data/second/inputs.txt', 'data/second/states.txt', 'data/second/transitions.txt', 'data/second/controls.txt', false, 'data/second/output.txt')
        this.first = this.firstCreator.getAutomat()
        this.second = this.secondCreator.getAutomat()
    }
    count = (input:Input) => {
        if (!this.lastSecondOutput) {
            this.lastSecondOutput = this.second.count(new Input('Y1', 0)).output
            this.secondCreator.writeFile()
        }

        let controlUpdate : Input[] = []
        let values = this.lastSecondOutput.getValues()
        for (let key in values) {
            controlUpdate.push(new Input(key, values[key] ? 1 : 0))
        }
        let result = this.first.count(input, controlUpdate)
        this.firstCreator.results.push(result)
        let firstOutput = result.output
        let secondResult = this.second.count(new Input('Y1', firstOutput.getValues().Y1 ? 2 : 0))
        this.lastSecondOutput = secondResult.output
        this.secondCreator.results.push(secondResult)

        this.firstCreator.writeFile()
        this.secondCreator.writeFile()
    }
}

let counter = new Counter()
for (let i = 0; i < 1000; i++) {
    let input = new Input('X' + rand(1, 8),InputValue.True)
    counter.count(input)
}